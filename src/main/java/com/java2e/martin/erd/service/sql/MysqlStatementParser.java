package com.java2e.martin.erd.service.sql;

import cn.fisok.pdman.dbreverse.Entity;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.mysql.ast.statement.MySqlCreateTableStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xiaobai
 *
 * MysqlStatementParser
 */
@Component("mysqlStatementParser")
public class MysqlStatementParser extends AbstractStatementParser {

    @Override
    public void parseStatement(List<SQLStatement> stmtList, List<Entity> entities) {
        for (SQLStatement sqlStatement : stmtList) {
            // 非Mysql建表语句则直接跳过
            if (!(sqlStatement instanceof MySqlCreateTableStatement)) {
                continue;
            }

            MySqlCreateTableStatement createTableStatement = (MySqlCreateTableStatement)sqlStatement;
            entities.add(buildEntity(createTableStatement));
        }
    }

}
