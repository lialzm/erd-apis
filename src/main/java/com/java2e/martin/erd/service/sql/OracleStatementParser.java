package com.java2e.martin.erd.service.sql;

import cn.fisok.pdman.dbreverse.Entity;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleCreateTableStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xiaobai
 *
 * OracleStatementParser
 */
@Component("oracleStatementParser")
public class OracleStatementParser extends AbstractStatementParser {

    @Override
    public void parseStatement(List<SQLStatement> stmtList, List<Entity> entities) {
        for (SQLStatement sqlStatement : stmtList) {
            // 非Oracle建表语句则直接跳过
            if (!(sqlStatement instanceof OracleCreateTableStatement)) {
                continue;
            }

            OracleCreateTableStatement createTableStatement = (OracleCreateTableStatement)sqlStatement;
            entities.add(buildEntity(createTableStatement));
        }
    }

}
