package com.java2e.martin.erd.config.token;

import com.java2e.martin.erd.SecurityConstant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @author xiaobai
 *
 */
@Configuration
public class TokenStoreConfig {

	/**
	 * jwt存储器，实际里面没有什么存储，因为jwt是自包含的，只要能解析jwt就行
	 * @return TokenStore
	 */
	@Bean
	public TokenStore jwtTokenStore() {
		return new JwtTokenStore(jwtAccessTokenConverter());
	}

	/**
	 * jwt转换器
	 *
	 * @return JwtAccessTokenConverter
	 */
	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter(){
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(SecurityConstant.JWT_SIGNING_KEY);
		return converter;
	}

	/**
	 * jwt增强
	 *
	 * @return TokenEnhancer
	 */
	@Bean
	public TokenEnhancer jwtTokenEnhancer(){
		return new JwtTokenEnhancer();
	}

}
