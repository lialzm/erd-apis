package com.java2e.martin.erd.controller;

import cn.fisok.pdman.dbreverse.ParseDataModel;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.java2e.martin.erd.entity.Project;
import com.java2e.martin.erd.service.ProjectService;
import com.java2e.martin.erd.service.sql.StatementProcessor;
import com.java2e.martin.erd.util.FileUtil;
import com.java2e.martin.erd.util.JsonUtil;
import com.java2e.martin.erd.util.Query;
import com.java2e.martin.erd.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


/**
 * <p>
 * PDMan全局配置表 前端控制器
 * </p>
 *
 * @author 狮少
 * @since 2020-10-26
 */
@Slf4j
@RestController
@RequestMapping("project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private StatementProcessor statementProcessor;

    /**
     * 添加
     *
     * @param map Map
     * @return ExecResult
     */
    @PostMapping("/save")
    public Result save(@RequestBody Map map) {
        QueryWrapper<Project> wrapper = new QueryWrapper<>();
        Object projectName = map.get("projectName");
        if (projectName == null) {
            return new Result<>().error("projectName为空");
        }
        wrapper.eq("project_name", projectName);
        Project selectOne = projectService.getOne(wrapper);
        Project project = new Project();

        try {
            project.setProjectName(projectName.toString());
            project.setConfigJSON(JsonUtil.generate(map.get("configJSON")).getBytes());
            project.setProjectJSON(JsonUtil.generate(map.get("projectJSON")).getBytes());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>().error(e.getMessage());
        }
        if (selectOne == null) {
            return new Result<>(projectService.save(project));
        } else {
            return new Result<>(projectService.update(project, wrapper));
        }
    }

    @GetMapping("/info/{projectId}")
    public Result projectService(@PathVariable String projectId) {
        return projectService.projectService(projectId);
    }


    /**
     * 删除
     *
     * @param project Project
     * @return ExecResult
     */
    @PostMapping("/delete")
    public Result<Boolean> removeById(@RequestBody Project project) {
        return new Result<>(projectService.removeById(project.getId()));
    }

    /**
     * 编辑
     *
     * @param project Project
     * @return ExecResult
     */
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody Project project) {
        project.setUpdatedTime(new Date());
        return new Result<>(projectService.updateById(project));
    }

    /**
     * 通过ID查询
     *
     * @param id String
     * @return Project
     */
    @GetMapping("/get/{id}")
    public Project getById(@PathVariable String id) {
        return projectService.getById(id);
    }

    /**
     * 分页查询
     *
     * @param params 分页以及查询参数
     * @return Page
     */
    @PostMapping("/page")
    public IPage getPage(@RequestBody Map params) {
        return projectService.page(new Query<>(params));
    }

    /**
     * 添加
     *
     * @param file sql文件
     * @param projectId 项目ID
     * @param dbType 数据库类型
     * @param moduleName 模块名称
     * @return Result
     */
    @PostMapping("/ddl/{projectId}")
    public Result ddl(MultipartFile file, @PathVariable String projectId, String dbType, String moduleName) {
        // parseStatements
        String content = FileUtil.fileToContent(file);
        List<SQLStatement> stmtList = SQLUtils.parseStatements(content, dbType);
        if (stmtList.size() == 0) {
            log.error("语法解析为空，请排查!");
            return new Result<>().error("语法解析为空，请排查!");
        }

        // build parseDataModel
        ParseDataModel parseDataModel = statementProcessor
                .processStatement(stmtList, dbType);

        // save module
        String moduleId = UUID.randomUUID().toString();
        String uuid = UUID.randomUUID().toString().substring(2, 7);
        moduleName = StringUtils.isEmpty(moduleName) ? String.format("DDL_%s_%s", dbType, uuid) : moduleName;
        return projectService.saveModule(projectId, parseDataModel, moduleName, moduleId);
    }
}

