package com.java2e.martin.erd.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @author 狮少
 * @version 1.0
 * @date 2020/10/26
 * @describtion JsonUtil
 * @since 1.0
 */
public final class JsonUtil {
    private static ObjectMapper mapper = new ObjectMapper();

    private JsonUtil() {
    }

    /**
     * Serialize any Java value as a String.
     */
    public static String generate(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    /**
     * Deserialize JSON content from given JSON content String.
     */
    public static <T> T parse(String content, Class<T> valueType) throws IOException {
        return mapper.readValue(content, valueType);
    }

    /**
     * Deserialize JSON content from given JSON content String.
     */
    public static JsonNode parse(String content) throws IOException {
        return mapper.readTree(content);
    }

    /**
     * Deserialize JSON content from given JSON content String.
     */
    public static JsonNode parseWithoutException(String content) {
        try {
            return mapper.readTree(content);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
